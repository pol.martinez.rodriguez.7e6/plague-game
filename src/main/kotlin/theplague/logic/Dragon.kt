package theplague.logic


import theplague.interfaces.Position

class Dragon ( size: Int, override val icon: String) : Colony(size){
    var timeToReproduce = 5

    override fun willReproduce(): Boolean {
        timeToReproduce -=1
        if (timeToReproduce == 0 && size < 3) {timeToReproduce = 4; return true}
        if (timeToReproduce == 0) timeToReproduce = 4
        return false
    }
    override fun reproduce() {
        size ++
    }
    override fun attacked(weapon: Weapon) {
        if (weapon is Hand || weapon is Escombra) return
        size -= 1
    }

    override fun needsToExpand(): Boolean {
        return size == 3
    }
    override fun expand(colony: Colony, position: Position, height: Int, width: Int): Colonozation {
        var x = (0 until width).random()
        var y = (0 until height).random()
        return Colonozation(Dragon( 1,"\uD83D\uDC09"), Position(x, y ))
    }

    override fun colonizedBy(plague: Colony): Colony? {
        if (plague is Ant) return null
        if (!needsToExpand()){
            size ++
            return null
        }
        return plague
    }

}