package theplague.logic

import theplague.interfaces.IPlayer
import theplague.interfaces.Iconizable
import theplague.interfaces.Position

class Player(
    val position: Position
) : IPlayer, Iconizable{
    private var currentPossition = Position(0,0)
    fun setCurrentPossition (currentPosition: Position) {
        currentPossition = currentPosition
    }
    fun getCurrentPossition (): Position {
        return currentPossition
    }
    override var turns: Int = 1
    override var livesLeft: Int = 14
    override var currentWeapon: Iconizable = Hand(3, "\uD83D\uDC46")
    override var currentVehicle: Iconizable = OnFoot(1,"\uD83D\uDEB6")
    override val icon: String
        get() = "\uD83D\uDEB6"
}