package theplague.logic

import theplague.interfaces.ITerritory
import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import java.util.*

class Territory (
    var plagueSize : Int
): ITerritory {
    var iconList = mutableListOf<Iconizable>()
    var hasPlayer = false
    var colony : Colony? = null
    fun setPlayer (iconizable: Iconizable) {
        if (!hasPlayer == true){
            hasPlayer = true
            iconList.add(iconizable)
        }
        else{
            hasPlayer = false
            iconList.remove(iconizable)
        }
    }
    fun addItem (item: Iconizable){
        iconList.add(item)
    }

    fun reproduce(){
        if (colony != null){
            when (colony){
                is Ant -> if ((colony as Ant).willReproduce()) {
                    (colony as Ant).reproduce()
                }
                is Dragon -> if ((colony as Dragon).willReproduce()) {
                    (colony as Dragon).reproduce()
                }
            }
        }
    }

    fun exterminate (weapon: Weapon){

        if (colony == null) return

        colony!!.attacked(weapon)

        if (colony!!.size <= 0) {
            colony = null
        }

    }
    override fun iconList(): List<Iconizable> {

        var icones: MutableList<Iconizable> = mutableListOf()

        for (i in iconList){
            if (i is Colony) {
                var aux = i
                for (j in 0 until aux.size) {
                    icones.add(Ant(1, 1, i.icon))
                }
            }else icones.add(i)
        }

        if (colony != null){
            for (i in 0 until colony!!.size){
                icones.add(colony!!)
            }
        }
        return icones
    }
/*
    fun expand( position: Position, width: Int, height: Int): Colonozation{
        var colonia = iconList.indexOfFirst { it is Colony }
        var colony = if (iconList[colonia] is Ant) Ant(1, 1, "\uD83D\uDC1C") else Dragon(1, "\uD83D\uDC09")
        return (iconList[colonia] as Colony).expand(colony, position, height, width)

    }
 */

    /*fun colonyInput(colony: Colony){
        var ant = iconList.indexOfFirst { it is Ant }
        var dragon = iconList.indexOfFirst { it is Dragon }
        when(colony){
            is Dragon -> {
                if (ant == -1 && dragon == -1) iconList.add(colony)
                else if (ant != -1){
                    iconList.remove(iconList[ant])
                    iconList.add(colony)
                }else{
                    if ((iconList[dragon] as Colony).willReproduce()) (iconList[dragon] as Colony).size ++
                }
            }
            is Ant -> {
                if (ant == -1 && dragon == -1) iconList.add(colony)
                else if (dragon == -1){
                    if ((iconList[ant] as Colony).willReproduce()) (iconList[ant] as Colony).size ++
                }
            }
        }
    }*/
}