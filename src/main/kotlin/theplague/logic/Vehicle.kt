package theplague.logic

import theplague.interfaces.Position

open class Vehicle(
    timesLeft: Int,
    icon: String) : Item(timesLeft, icon) {

    fun canMove(from : Position, to: Position) : Boolean {
        return false
    }
}