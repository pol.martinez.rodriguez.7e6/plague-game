package theplague.logic

open class Weapon(
    timesLeft: Int,
    icon: String) : Item(timesLeft, icon) {
}