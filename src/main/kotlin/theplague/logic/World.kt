package theplague.logic

import theplague.interfaces.*
import java.lang.reflect.Array.get
import kotlin.reflect.typeOf
import kotlin.system.exitProcess


class World(override val width:Int, override val height: Int): IWorld {
    override var territories: List<List<Territory>> = MutableList(height){ MutableList(width){Territory(0)} }
    var currentTerritory = territories[width/2][height/2]
    override var player = Player(Position(width/2,height/2))
    var liveTimeVehicle = 0
    init {
        player.setCurrentPossition(Position(width/2,height/2))
        currentTerritory.setPlayer(player)
    }

    private fun createPos(numSobre100: Int): Boolean{

        var response = mutableListOf<Boolean>()

        for (i in 1..numSobre100) response.add(true)
        for (i in 1..(100-numSobre100)) response.add(false)

        return response.random()

    }

    override fun nextTurn() {
        player.turns += 1
        generateNewItems()
        if (player.currentVehicle !is OnFoot) liveTimeVehicle += 1
        if (liveTimeVehicle == 6) {
            player.currentVehicle = OnFoot(Int.MAX_VALUE, "\uD83D\uDEB6")
            liveTimeVehicle = 0
        }
        reproduce()
        generateNewColonies()
        expand()
    }
    fun reproduce () {
        for (i in territories){
            for (j in i){
                j.reproduce()
            }
        }
    }
    fun checkIfThereAreItems(position: Position): Boolean{
        val items = mutableListOf<String>("\uD83D\uDEB2", "\uD83D\uDE81", "\uD83E\uDDF9", "\uD83D\uDDE1")
        val lista = territories[position.y][position.x].iconList()
        if (lista.isEmpty()) return false
        for (i in lista){
            if (i.icon in items) return true
        }
        return false
    }
    fun generateNewColonies () {
        var territori: Territory
        var x: Int
        var y: Int
        do{
            x = (0 until width).random()
            y = (0 until height).random()

            territori = territories[y][x]

        }while(territori.hasPlayer || territori.plagueSize != 0)
        if (createPos(30)) {
            if (territories[y][x].colony == null) {
                territories[y][x].colony = Ant(30, 1, "\uD83D\uDC1C")
            }
        }
        else if (createPos(10)) {
            if (territories[y][x].colony == null || territories[y][x].colony is Ant ) {
                territories[y][x].colony = Dragon(1, "\uD83D\uDC09")
            }
        }
        territories[y][x].plagueSize = 1
    }
    fun generateNewItems() {
        val rangs = mutableListOf<IntRange>((0 until 25),(25 until 35),(35 until 60),(60 until 70))
        val items = mutableListOf(Bicycle(5,"\uD83D\uDEB2"), Helicopter(5,"\uD83D\uDE81"), Escombra(Int.MAX_VALUE, "\uD83E\uDDF9"), Espasa(Int.MAX_VALUE, "\uD83D\uDDE1"))
        val numberRandom = (0 until 100).random()
        if (numberRandom in (0 until 70)){
            lateinit var item : Iconizable
            for (i in rangs){
                if (numberRandom in i) item= items[rangs.indexOf(i)]
            }
            var xPosition = 0
            var yPosition = 0
            do {
                xPosition = (territories[0].indices).random()
                yPosition = (territories.indices).random()
            } while (checkIfThereAreItems(Position(xPosition,yPosition)))
            territories[yPosition][xPosition].addItem(item)
        }
    }
    override fun gameFinished(): Boolean {
        if (player.livesLeft == 0){
           return true
        }
       return false
    }

    override fun canMoveTo(position: Position): Boolean {
        val currentPossition = player.getCurrentPossition()

        when (player.currentVehicle.icon){

            "\uD83D\uDEB6" -> { // a peu
                val x = Math.abs(currentPossition.x - position.x)
                val y = Math.abs(currentPossition.y - position.y)

                return ((x == 0 || x == 1) && (y == 0 || y == 1))
                return false
            }

            "\uD83D\uDEB2" -> { // a bici
                val x = Math.abs(currentPossition.x - position.x)
                val y = Math.abs(currentPossition.y - position.y)

                return (x in (0..5) && y in (0..5))
                return false
            }
            else -> return true
        }
    }

    override fun moveTo(position: Position) {
        currentTerritory.setPlayer(player)
        player.setCurrentPossition(position)
        currentTerritory = territories[position.y][position.x]
        currentTerritory.setPlayer(player)
    }
    override fun exterminate() {
        val position = player.getCurrentPossition()
        territories[position.y][position.x].exterminate(player.currentWeapon as Weapon)
    }

    override fun takeableItem(): Iconizable? {

        val posicio = player.getCurrentPossition()

        if (!territories[posicio.y][posicio.x].iconList.isEmpty()){

            for (i in territories[posicio.y][posicio.x].iconList){
                when (i.icon){
                    "\uD83D\uDEB2" -> {
                        return Vehicle(3, i.icon)
                    }
                    "\uD83D\uDE81" -> {
                        return Vehicle(3, i.icon)
                    }
                    "\uD83D\uDDE1" -> {
                        return Weapon(3, i.icon)
                    }
                    "\uD83E\uDDF9" -> {
                        return Weapon(3, i.icon)
                    }
                }
            }
        }
        return null;
    }

    override fun takeItem() {

        val items = mutableListOf<String>("\uD83D\uDEB2", "\uD83D\uDE81", "\uD83E\uDDF9", "\uD83D\uDDE1")

        val posicio = player.getCurrentPossition()

        val territori = territories[posicio.y][posicio.x]

        var icon: Iconizable = territori.iconList[0]

        for (i in territori.iconList){
            if (i.icon in items) {
                if (i is Vehicle) liveTimeVehicle = 0
                icon = i
                break // ho sento
            }
        }

        when (icon.icon){

            "\uD83D\uDEB2" -> {
                player.currentVehicle = Bicycle(5, icon.icon)
            }
            "\uD83D\uDE81" -> {
                player.currentVehicle = Helicopter(5, icon.icon)
            }
            "\uD83D\uDDE1" -> {
                player.currentWeapon = Espasa(-1, icon.icon)
            }
            "\uD83E\uDDF9" -> {
                player.currentWeapon = Escombra(-1, icon.icon)
            }
        }
        territories[posicio.y][posicio.x].iconList.remove(icon)

    }

    fun expand(){
        lateinit var colonization: Colonozation
        for (i in territories.indices){
            for (j in territories[i].indices){
                /*
                val colony = territories[i][j].expand(Position(i, j), width, height)
                territories[colony.position.y][colony.position.x].iconList.add(colony.colony)
                 */
                if (territories[i][j].colony != null){
                    when (territories[i][j].colony!!) {
                        is Ant -> {
                            val hormiga = territories[i][j].colony!! as Ant
                            if (hormiga.needsToExpand()){
                                colonization = hormiga.expand(hormiga,Position(i,j),10,10)
                                place(colonization)
                                if (player.livesLeft - 1 >= 0) player.livesLeft -= 1
                                break
                            }
                        }
                        is Dragon -> {
                            val dragon = territories[i][j].colony!! as Dragon
                            if (dragon.needsToExpand()){
                                colonization = dragon.expand(dragon,Position(i,j),10,10)
                                place(colonization)
                                if (player.livesLeft - 1 >= 0) player.livesLeft -= 1
                                break
                            }
                        }
                    }


                }
            }
        }
    }
    private fun place (colonization: Colonozation) {
        var res: Colony? = null
        val colonyColonizated = territories[colonization.position.y][colonization.position.x].colony
        if (colonyColonizated != null) {
            when(colonyColonizated){
                is Ant -> {
                    val hormiga = territories[colonization.position.y][colonization.position.x].colony!! as Ant
                    res = hormiga.colonizedBy(colonization.colony)
                }
                is Dragon -> {
                    val drac = territories[colonization.position.y][colonization.position.x].colony!! as Dragon
                    res = drac.colonizedBy(colonization.colony)
                }
            }
        }
        else territories[colonization.position.y][colonization.position.x].colony = colonization.colony
        if (res != null){
            territories[colonization.position.y][colonization.position.x].colony = res
        }
    }
}